# GitService

A simple service written in C# which fetches & fast-forwards a git repo and then runs a command.

I made this for my website (kortlepel.com), so that changes which are pushed to the repo are pulled to the server and the webserver is restarted if necessary.

Example systemd service file GitService.service:

```systemd
[Unit] 
Description=GitService for kortlepel.com

[Service] 
Restart=always
WorkingDirectory=/root/GitService
ExecStart=mono GitService.exe "/var/www/kortlepel-com" "cd /var/www/kortlepel-com/ && HOME=/root go run generator.go"

[Install] 
WantedBy=multi-user.target
```

The command specified changes directory into the website's folder, and runs a go program which renders the templated html.

## How it works

Every 5-60 seconds (random), fetches and fast-forwards the repository's current branch. If a fast-forward happened, runs the specified command.

It uses libgit for any calls to git.
