﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using LibGit2Sharp;
using NLog;
using NLog.Fluent;

namespace GitService
{
    internal class Program
    {
        private static Logger m_Logger = LogManager.GetCurrentClassLogger();

        public static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                m_Logger.Error("Invalid arguments, expected <repo path> <command>");
                return;
            }

            var gitPath = args[0];
            var command = args[1];

            m_Logger.Debug($"repo path: '{gitPath}'");
            m_Logger.Debug($"command: '{command}'");


            m_Logger.Info("Opening repo");
            var repo = new Repository(gitPath);
            if (!repo.Head.IsTracking)
            {
                m_Logger.Error("Current branch is not tracking a remote branch, can't pull");
                return;
            }

            m_Logger.Info("Repo opened successfully");

            var rand = new Random();
            
            while (true)
            {
                m_Logger.Info("Checking repo status");
                
                var latestCommitBefore = repo.Head.Commits.First();
                var result = Commands.Pull(repo, new Signature("GitService", "gitservice@localhost", DateTimeOffset.Now),
                    new PullOptions()
                    {
                        FetchOptions = new FetchOptions(),
                        MergeOptions = new MergeOptions() { FastForwardStrategy = FastForwardStrategy.FastForwardOnly }
                    });
                switch (result.Status)
                {
                    case MergeStatus.UpToDate:
                        m_Logger.Debug("Everything up-to-date");
                        break;
                    case MergeStatus.FastForward:
                        m_Logger.Info(
                            $"New commits, fast forwarded from {latestCommitBefore} to {repo.Head.Commits.First()}");
                        var shortMsg = repo.Head.Commits.First().MessageShort;
                        m_Logger.Info($"Now at '{shortMsg}'");
                        m_Logger.Info("Running specified command");
                        var process = Process.Start("bash", $"-c '{command}'");
                        if (process == null)
                        {
                            m_Logger.Error("Failed to start process");
                        }
                        else
                        {
                            process.WaitForExit();
                            if (process.ExitCode != 0)
                            {
                                m_Logger.Error($"Command returned non-zero exit code: {process.ExitCode}");
                            }

                            m_Logger.Debug($"Running command took {process.ExitTime - process.StartTime}");
                        }
                        break;
                    case MergeStatus.NonFastForward:
                        m_Logger.Warn("Did not fast-forward");
                        break;
                    case MergeStatus.Conflicts:
                        m_Logger.Error("There were conflicts!");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                // sleep between 5 and 60 seconds
                Thread.Sleep(rand.Next(5_000, 60_000));
            }
        }
    }
}